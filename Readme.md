# Rabbitmq 使用示例
Rabbitmq学习示例

#### 项目结构说明
> RabbitMQ.ClientDemos

RabbitMQ.Client的示例项目：  
* 1.1SimpleSend：简单发送
* 1.2SimpleReceive：简单接收

> EasyNetQ.Demos

EasyNetQ的使用示例
* EasyNetQTest.Message: 发送的消息对象定义，公共项目
* EasyNetQTest.Publisher, EasyNetQTest.Subscriber: 消息的 发布、订阅模式示例。发送到交换机，使用Topic模式
* EasyNetQTest.Send, EasyNetQTest.Recieve: 消息的 直接发送、接收模式。直接发送到队列
* EasyNetQTest.PublisherDelayed：发布延时消息，使用RabbitMQ的延时插件
* EasyNetQTest.ErrorHandler：消息消费时的异常处理示例。
* Topshelf.MQ.Revice： 使用Topshelf开发的Windows服务消费端，支持多个消费者
* EasyNetQTest.AspnetCore： 使用AspNet Core开发的消费端，支持多个消费者

#### 相关知识和资料

> 消息队列介绍及常见消息队列介绍 https://cloud.tencent.com/developer/article/1006035   

> Rabbitmq相关资源:
1. 官网: http://www.rabbitmq.com/
2. Github上找到的Rabbitmq中文资料 http://rabbitmq.mr-ping.com/



## 1 官方客户端 RabbitMQ.Client 学习
> 安装组件
```C#
//RabbitMQ客户端
install-package RabbitMQ.Client

//RabbitMQ客户端 DI集成
//安装这个就不需要安装 RabbitMQ.Client 了
install-package RabbitMQ.Client.Core.DependencyInjection
```
RabbitMQ.Client 封装了完整的RabbitMQ客户端功能，但使用起来比较麻烦，需要进行进一步的封装。
EasyNetQ是基于RabbitMQ.Client的封装，多数场景下EasyNetQ就可以满足。

> 相关资料和示例:

1. 官方客户端链接: http://www.rabbitmq.com/dotnet.html  
   GitHub仓库: https://github.com/rabbitmq/rabbitmq-dotnet-client
2. RabbitMQ 官方.NET CORE教程实操演练 https://github.com/sheng-jie/RabbitMQ

3. 教程1 https://www.tinymind.net.cn/articles/e1bedc46592721
4. 教程2 https://www.cnblogs.com/stulzq/p/7551819.html
5. 教程3 https://www.cnblogs.com/shanfeng1000/p/12133181.html

## 2 EasyNetQ 学习
> 安装组件
```c#
Install-Package EasyNetQ
```

> EasyNetQ:快速使用RabbitMQ的.Net组件, 参看: 

1. http://easynetq.com  
2. https://github.com/EasyNetQ/EasyNetQ
3. https://github.com/EasyNetQ/EasyNetQ/wiki/Introduction
4. EasyNetQ手册翻译 https://www.cnblogs.com/HuangLiang/category/1027432.html
5. EasyNetQ手册翻译 https://www.cnblogs.com/forcesoul/p/7976183.html
6. EasyNetQ异常处理 https://blog.csdn.net/aofgryq715157389/article/details/101626185
6. 异常消息处理 https://www.cnblogs.com/IWings/p/14681745.html
   
> EasyNetQ的示例:  

1. https://github.com/mikehadlow/EasyNetQTest
2. http://www.bubuko.com/infodetail-2542681.html
3. https://www.cnblogs.com/struggle999/p/6937530.html   
4. https://www.cnblogs.com/dandan123/p/10097711.html , https://github.com/Cglvzh/NetCore.EasyNetQAdvance.Demo.git

> EasyNetQ连接字符串信息

* host，host=localhost 或者host=192.168.1.102,如果用到集群配置的话，那么可以用逗号将服务地址隔开，例如host=a.com,b.com,c.com
* virtualHost,虚拟主机，默认为‘/‘
* username,用户登录名
* password，用户登录密码
* requestedHeartbeat,心跳设置，默认是10秒
* prefetchcount，默认是50
* pubisherConfirms，默认为false
* persistentMessages，消息持久化，默认为true
* timeout，默认为10秒


## 3 使用Topshelf 搭建 Windows 服务

  可以使用Topshelf来把控制台程序改造成Windows服务程序
>安装组件
```
Install-Package Topshelf
Install-Package Topshelf.Log4Net
```

>教程、示例
1. https://blog.csdn.net/hany3000/article/details/83619283
2. https://www.cnblogs.com/swjian/p/11498808.html
3. https://www.cnblogs.com/zhurong/p/10311750.html
