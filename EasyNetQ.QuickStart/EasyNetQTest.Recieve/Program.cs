﻿using EasyNetQ;
using EasyNetQTest.Message;
using System;
using System.Threading;

namespace EasyNetQTest.Recieve
{
    /// <summary>
    /// 直接发送到队列模式
    /// 消息接收者
    /// </summary>
    class Program
    {
        static void Main(string[] args)
        {
            //var connStr = "host=192.168.0.248;username=admin;password=admin;prefetchcount=10";
            var connStr = "host=43.228.77.213;username=admin;password=Sa@Os0928;prefetchcount=10";
            int delay = 50;
            using (var bus = RabbitHutch.CreateBus(connStr))
            {
                //直接从队列中接收消息
                bus.SendReceive.Receive<MyMessage>("Test.TestQueue2", o =>
                {
                    Console.WriteLine("收到消息: {0}", o.MsgContent);

                    Thread.Sleep(delay);//模拟耗时,进行消息处理                        
                    Console.WriteLine("消息处理结束\n");
                });

                //程序不结束，等待输入
                Console.WriteLine("消息接收已启动\n输入 [enter] 退出程序.\n");
                Console.ReadLine();
            }
        }
    }
}
