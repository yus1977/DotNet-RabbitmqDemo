﻿using EasyNetQ;
using EasyNetQTest.Message;
using System;

namespace EasyNetQTest.Publisher
{
    /// <summary>
    /// 发布订阅topic模式,
    /// 消息发布者
    /// </summary>
    class Program
    {
        static void Main(string[] args)
        {
            //var connStr = "host=192.168.80.71;virtualHost=EDCVHOST;username=admin;password=edison";
            var connStr = "host=43.228.77.213;username=admin;password=Sa@Os0928;publisherConfirms=true";
            //var connStr = "host=192.168.0.248;username=admin;password=admin;publisherConfirms=true";
            int msgCout = 1000;
            string msg = "EasyNetQTest.Publisher,发送的消息";

            //连接mq
            using (var bus = RabbitHutch.CreateBus(connStr))
            {

                //循环发送消息
                for (int i = 0; i < msgCout; i++)
                {
                    var message = msg + i;
                    var msgModel = new MyMessage
                    {
                        MsgId = i,
                        MsgContent = message
                    };

                    //Publish模式，需要先进行一次订阅
                    //在订阅时会创建订阅的队列。然后再发布消息时才会发到订阅的队列中

                    //消息发布,使用交换机topic方式发布，队列名为Test.TestQueue*
                    //消费者使用Subscribe来接收
                    bus.PubSub.Publish(msgModel);

                    Console.WriteLine(" 发布消息: {0}", message);
                }

                Console.WriteLine("\n发布结束\n输入 [enter] 退出程序.\n");
                //程序不结束，等待输入
                Console.ReadLine();
            }
        }
    }
}
