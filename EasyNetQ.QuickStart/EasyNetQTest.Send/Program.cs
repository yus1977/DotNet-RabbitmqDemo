﻿using EasyNetQ;
using EasyNetQTest.Message;
using System;

namespace EasyNetQTest.Send
{
    /// <summary>
    /// 直接发送到队列模式
    /// 消息发送者
    /// </summary>
    class Program
    {
        static void Main(string[] args)
        {
            //var connStr = "host=192.168.0.248;username=admin;password=admin;prefetchcount=10";
            var connStr = "host=43.228.77.213;username=admin;password=Sa@Os0928;prefetchcount=10";
            int msgCout = 1000;
            string msg = "发送的消息";

            //连接mq
            using (var bus = RabbitHutch.CreateBus(connStr))
            {

                //循环发送消息
                for (int i = 0; i < msgCout; i++)
                {
                    var message = msg + i;
                    var msgModel = new MyMessage
                    {
                        MsgId = i,
                        MsgContent = message
                    };

                    //发送消息
                    //直接发送到指定队列,会忽略MyMessageg上的[Queue]特性
                    bus.SendReceive.Send("Test.TestQueue2",msgModel);
                    
                    Console.WriteLine(" 发送消息: {0}", message);
                }

                Console.WriteLine("\n发送结束\n输入 [enter] 退出程序.\n");
                //程序不结束，等待输入
                Console.ReadLine();
            }
        }
    }
}
