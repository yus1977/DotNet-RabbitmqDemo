﻿using EasyNetQ;
using EasyNetQTest.Message;
using System;
using System.Threading;

namespace EasyNetQTest.Subscriber
{
    /// <summary>
    /// 发布订阅topic模式,
    /// 消息订阅者
    /// </summary>
    class Program
    {
        static void Main(string[] args)
        {
            //var connStr = "host=192.168.0.248;username=admin;password=admin";
            var connStr = "host=43.228.77.213;username=admin;password=Sa@Os0928";
            int delay = 50;
            using (var bus = RabbitHutch.CreateBus(connStr))
            {
                //消息订阅
                //订阅时产生订阅的队列,队列名为Test.TestQueue_sub1
                bus.PubSub.Subscribe<MyMessage>("sub1", o=>
                {
                    Console.WriteLine("收到消息: {0}", o.MsgContent);

                    Thread.Sleep(delay);//模拟耗时,进行消息处理                        
                    Console.WriteLine("消息处理结束\n");
                });

                //程序不结束，等待输入
                Console.WriteLine("消息订阅已启动\n输入 [enter] 退出程序.\n");
                Console.ReadLine();
            }
        }
    }
}
