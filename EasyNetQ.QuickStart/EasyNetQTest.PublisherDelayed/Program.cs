﻿using System;
using EasyNetQ;
using EasyNetQTest.Message;

namespace EasyNetQTest.PublisherDelayed
{
    /// <summary>
    /// 发布订阅topic模式,延时发布
    /// 消息发布者
    /// </summary>
    class Program
    {
        static void Main(string[] args)
        {
            //var connStr = "host=192.168.0.248;username=admin;password=admin;publisherConfirms=true";
            var connStr = "host=43.228.77.213;username=admin;password=Sa@Os0928;publisherConfirms=true";
            int msgCout = 100;
            string msg = "EasyNetQTest.PublisherDelayed,延时发送的消息";

            //连接mq
            //使用RabbitMQ延迟消息插件
            //using (var bus = RabbitHutch.CreateBus(connStr, x => x.Register<IScheduler, DelayedExchangeScheduler>()))
            using (var bus = RabbitHutch.CreateBus(connStr, x => x.EnableDelayedExchangeScheduler()))
            {

                //循环发送消息
                for (int i = 0; i < msgCout; i++)
                {
                    var message = msg + i;
                    var msgModel = new MyMessage
                    {
                        MsgId = i,
                        MsgContent = message
                    };

                    //Publish模式，需要先进行一次订阅
                    //在订阅时会创建订阅的队列。然后再发布消息时才会发到订阅的队列中

                    //消息发布,使用交换机topic方式发布，队列名为Test.TestQueue*
                    //延时发布,时间为UTC时间
                    //这里是延时10秒发送消息
                    bus.Scheduler.FuturePublish(msgModel,TimeSpan.FromSeconds(10));

                    Console.WriteLine(" 发布消息: {0}", message);
                }

                Console.WriteLine("\n发布结束\n输入 [enter] 退出程序.\n");
                //程序不结束，等待输入
                Console.ReadLine();
            }
        }
    }
}