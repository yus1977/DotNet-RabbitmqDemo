﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EasyNetQTest.AspnetCore.Models
{
    /// <summary>
    /// MQ消息内容
    /// </summary>
    public class MQMessage
    {
        public long MsgId { get; set; }
        public string MsgType { get; set; }
        public string MsgContent { get; set; }
    }
}
