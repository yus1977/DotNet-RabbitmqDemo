﻿using EasyNetQTest.AspnetCore.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EasyNetQTest.AspnetCore.Controllers
{
    /// <summary>
    /// MQ发送API接口
    /// </summary>
    [Route("mqsend")]
    public class MQSendController
    {
        MQSendService sendService;
        public MQSendController(MQSendService sendService)
        {
                this.sendService = sendService;
        }

        [HttpGet("w/{count}")]
        public async Task<string> SendMessageWaitAsync(int count = 1)
        {
            await sendService.SendMessageWaitAsync(count);
            return "Send OK";
        }

        [HttpGet("n/{count}")]
        public async Task<string> SendMessageNoWaitAsync(int count = 1)
        {
            await sendService.SendMessageNoWaitAsync(count);
            return "Send OK";
        }
    }
}
