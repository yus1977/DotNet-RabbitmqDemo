﻿using System;
using Topshelf;

namespace Topshelf.MQ.Revice
{
    /// <summary>
    /// 程序可以安装成windows服务运行,运行 XXX.exe install 即可
    /// </summary>
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                // 配置和运行宿主服务
                HostFactory.Run(x =>                                 //1
                {
                    x.Service<MyService>(s =>                        //2
                    {
                        // 指定服务类型。这里设置为 Service
                        s.ConstructUsing(name => new MyService());     //3

                        // 当服务启动后执行什么
                        s.WhenStarted(tc => tc.Start());              //4

                        // 当服务停止后执行什么
                        s.WhenStopped(tc => tc.Stop());               //5
                    });

                    // 服务用本地系统账号来运行
                    x.RunAsLocalSystem();                            //6

                    // 服务描述信息
                    x.SetDescription("RabbitMQ 消息接收者测试服务");        //7
                    // 服务显示名称
                    x.SetDisplayName("MyProjectServiceShowName");                       //8
                    // 服务名称
                    x.SetServiceName("MyProjectService");                       //9 
                });
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }
    }
}
