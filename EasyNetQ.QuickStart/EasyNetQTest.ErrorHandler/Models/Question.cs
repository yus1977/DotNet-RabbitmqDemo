﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EasyNetQErrorHandler.Models
{
    public class Question
    {
        public string Text { get; }

        public Question(string text)
        {
            Text = text;
        }
    }
}
