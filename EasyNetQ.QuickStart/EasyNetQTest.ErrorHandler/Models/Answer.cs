﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EasyNetQErrorHandler.Models
{
    public class Answer
    {
        public string Text { get; }

        public Answer(string text)
        {
            Text = text;
        }
    }
}
