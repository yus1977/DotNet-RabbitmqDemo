﻿using EasyNetQ.Consumer;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace EasyNetQErrorHandler
{
    /// <summary>
    /// EasyNetQ消息异常处理
    /// </summary>
    public sealed class AlwaysRequeueErrorStrategy : IConsumerErrorStrategy
    {
        public void Dispose()
        {
        }

        /// <summary>
        /// 处理消费异常
        /// </summary>
        /// <param name="context"></param>
        /// <param name="exception"></param>
        /// <returns></returns>
        public AckStrategy HandleConsumerError(ConsumerExecutionContext context, Exception exception)
        {
            //处理异常
            Console.WriteLine("Process Exception, Delay Retry!");
            //延时3秒再处理，实现了延时重试的效果
            Thread.Sleep(1 * 1000);

            //可以按下面三种方式来进行处理

            //不回复Ack给MQ,把消息重新发到队列中, 可以重新消费消息
            return AckStrategies.NackWithRequeue;

            //不回复Ack给MQ,消息不重新发到队列中, 如果有死信配置,则消息超时后成为死信,
            //return AckStrategies.NackWithoutRequeue;

            //回复Ack给MQ, MQ会认为消息已消费成功
            //return AckStrategies.Ack;
        }

        /// <summary>
        /// 处理消费者取消处理
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public AckStrategy HandleConsumerCancelled(ConsumerExecutionContext context)
        {
            Console.WriteLine("Process Consumer Cancelled, Delay Retry!");
            //延时3秒再处理，实现了延时重试的效果
            Thread.Sleep(1 * 1000);

            //可以按下面三种方式来进行处理

            //不回复Ack给MQ，把消息重新发到队列中 , 可以重新消费消息
            return AckStrategies.NackWithRequeue;

            //不回复Ack给MQ,消息不重新发到队列中, 如果有死信配置,则消息超时后成为死信,
            //return AckStrategies.NackWithoutRequeue;

            //回复Ack给MQ, MQ会认为消息已消费成功
            //return AckStrategies.Ack;
        }
    }
}
