﻿using EasyNetQ;
using System;

namespace EasyNetQTest.Message
{
    /// <summary>
    /// 自定义消息对象
    /// 定义了队列和交换机,交换机默认为topic模式
    /// </summary>
    [Queue("Test.TestQueue", ExchangeName = "TestQueue.Exchange")]
    public class MyMessage
    {
        public long MsgId { get; set; }
        public string MsgContent { get; set; }
    }

    public class MyMessage2
    {
        public long MsgId { get; set; }
        public string MsgContent { get; set; }
    }

    public class Consts
    {
        public const string QueueName = "Test.TestQueue";
    }
}
